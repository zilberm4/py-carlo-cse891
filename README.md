# Install
```bash
git clone https://gitlab.msu.edu/zilberm4/py-carlo-cse891.git pycarlo
cd pycarlo
python3 -m venv venv
source venv/bin/activate
pip3 install -r requirements.txt
```

# Usage
```bash
# train
python3 main.py
python3 main.py --mode train 

# resume training
python3 main.py --resume --checkpoint 'checkpoint.pt'  
python3 main.py -r -c 'checkpoint.pt'

# test
python3 main.py --mode test --scene merge --scenario ego --n-evals 5

# manual play
python3 main.py --mode manual

# plot sb3 train logs
python3 main.py --mode plot --data 'run_123.csv'

# view additional options
python3 main.py --help
```

# HPCC docs
```bash
# ssh into <user>@<hpcc-node> (ex: dev-amd20-v100)
git clone https://gitlab.msu.edu/zilberm4/py-carlo-cse891.git pycarlo
cd pycarlo

# prepares environment (loads modules, create and activate venv, installs reqs)
source ./hpcc/init.sh

# run training session
./hpcc/run.sh

# render eval GIF after offscreen training
python3 main.py --mode sbt --offscreen --record -c <ckpt.zip>
```

# Submit HPCC slurm job
1. Edit project paths in `hpcc/job.sbatch`
2. Update command on last line of `job.sbatch` if needed.
3. Submit Job: 
```bash
# ~/pycarlo/
./hpcc/slurm.sh
```

# All Options
```bash
usage: main.py [-h] [--mode MODE] [--data DATA_PATH] [--checkpoint CHECKPOINT] [-r] [--offscreen] [--record] [--output-prefix OUTPUT_PREFIX] [--save-freq SAVE_FREQ] [--total-timesteps TOTAL_TIMESTEPS] [--tensorboard] [--frame-stack STACKED_BUFFER_SIZE] [--learning_rate LEARNING_RATE]
               [--n_steps N_STEPS] [--batch_size BATCH_SIZE] [--n_epochs N_EPOCHS] [--gamma GAMMA] [--gae_lambda GAE_LAMBDA] [--clip_range CLIP_RANGE] [--ent_coef ENT_COEF] [--vf_coef VF_COEF]

optional arguments:
  -h, --help            show this help message and exit
  --mode MODE, -m MODE  train, test, manual, plot
  --scene SCENE, --scene SCENE merge, curve
                        Specifies which traffic scene to load.
  --data DATA_PATH, -d DATA_PATH
                        Path to .csv log file for plotting.
  --checkpoint CHECKPOINT, --model CHECKPOINT, -c CHECKPOINT, --ckpt CHECKPOINT
                        Enter path of model .pth file
  -r, --resume          Resume training from checkpoint
  --offscreen           Pass this flag to render offscreen
  --record              Record video of gameplay
  --output-prefix OUTPUT_PREFIX
                        Add prefix to output dir.
  --save-freq SAVE_FREQ
                        Save model frequency
  --total-timesteps TOTAL_TIMESTEPS
                        Total timesteps
  --tensorboard         Enable tensorboard
  --frame-stack STACKED_BUFFER_SIZE
                        Number of frames to stack per observation. Default is 1.
  --learning_rate LEARNING_RATE
                        Learning rate
  --n_steps N_STEPS     Number of steps per rollout
  --batch_size BATCH_SIZE
                        Batch size
  --n_epochs N_EPOCHS   Number of epochs
  --gamma GAMMA         Discount factor
  --gae_lambda GAE_LAMBDA
                        GAE lambda
  --clip_range CLIP_RANGE
                        Clipping range
  --ent_coef ENT_COEF   Entropy coefficient
  --vf_coef VF_COEF     Value function coefficient
```