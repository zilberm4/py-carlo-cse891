#!/bin/bash

echo "[STATUS] Loading Modules"
module load git/2.27.0
module load GCCcore/8.3.0
module load Python/3.8.3

# check for venv folder
echo "[STATUS] Activating Venv"
if [[ "$VIRTUAL_ENV" != "" ]]
then
  :
else
  python3 -m venv venv
fi
# activate
source venv/bin/activate

# checks reqs
# echo "[STATUS] Checking Reqs..."
# python3 -c "import pkg_resources; pkg_resources.require(open('requirements.txt',mode='r'))" &>/dev/null || pip3 install --ignore-installed -r requirements.txt

echo "[SUCCESS] Env Ready."