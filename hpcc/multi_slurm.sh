#!/bin/bash

BASE_SCRIPT='#!/bin/bash --login

#SBATCH --time=300
#SBATCH --nodes=1
#SBATCH --gres=gpu:v100s:1
#SBATCH --ntasks=1
#SBATCH --mem=32GB
#SBATCH --job-name pycarlo_n_epochs_{REPLACEME}
#SBATCH --output /mnt/home/zilberm4/Documents/code/research/py-carlo-cse891/hpcc/logs/log_%j.log

echo "JobID: $SLURM_JOB_ID"
# echo "Running on node: `hostname`"

module purge
module load GCCcore/8.3.0
module load Python/3.8.3

PROJDIR=/mnt/home/zilberm4/Documents/code/research/py-carlo-cse891/
cd ${PROJDIR}
source venv/bin/activate
'

param="n_epochs"

values=(1024 2048 3072 4096)

tmp_job="hpcc/_tmp_job.sbatch"

for i in "${values[@]}"
do
    tmp_base=${BASE_SCRIPT//\{REPLACEME\}/$i}
    echo -e "$tmp_base\npython3 main.py --offscreen --$param $i --output-prefix $param" > $tmp_job
    sbatch $tmp_job  
    rm $tmp_job
done

