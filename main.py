import os

os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = "hide"

import numpy as np
import pygame
import tqdm

from src.network.manual import play
from src.settings import Config
from importlib import reload
from src.utils import ensure_dir_structure, get_unique_filename
from src.network.train import train
from src.network.test import test
from src.network.evaluate import evaluate
from src.utils import parse_args

from src.plot import plot_multiple_sb_data


if __name__ == "__main__":
    args = parse_args()
    config = Config(args)
    
    if args.mode == "train":
        ensure_dir_structure(config.EXPERIMENT_DIR)
        train(config, args)

    elif args.mode == "test":
        # ensure_dir_structure(config.EXPERIMENT_DIR)
        test(config, args)

    elif args.mode == "eval":
        evaluate(config, args)

    elif args.mode == "manual":
        play()


    elif args.mode == "plot":
        plot_multiple_sb_data([args.data_path], [None])

    pygame.quit()
