#!/bin/bash
# removes empty experiment folders

dir=$1

for subdir in "$dir"/*; do
  if [ -d "$subdir" ] && [ -z "$(ls -A "$subdir")" ]; then
    rmdir "$subdir"
  fi
done
