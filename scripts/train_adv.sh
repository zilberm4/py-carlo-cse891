#!/bin/bash

python3 main.py --scenario adv --learning_rate .0001 --n_steps 2048 --batch_size 256 --n_epochs 32 --gamma .99 --clip_range .1 --ent_coef .02 --output-prefix base_adv --tensorboard 