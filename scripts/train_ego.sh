#!/bin/bash
python3 main.py --learning_rate .0001 --n_steps 2048 --batch_size 256 --n_epochs 32 --total-timesteps 500000 --gamma .99 --clip_range .1 --ent_coef .01 --output-prefix base_ego --tensorboard
