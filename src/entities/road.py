import numpy as np
import pygame
from pygame import gfxdraw
from pygame.math import Vector2

from src.utils import draw_line_dashed, intersect


class Road:
    def __init__(self, config, points):
        self.config = config
        self.points = points
        self.lane_markings = []

    def add_lane_markings(self, lane_marking):
        self.lane_markings.append(lane_marking)

    def draw(self, screen):
        pygame.draw.polygon(screen, (0, 0, 0), self.points, 10)
        pygame.gfxdraw.filled_polygon(screen, self.points, (60, 60, 60))
        for lane_marking in self.lane_markings:
            draw_line_dashed(screen, (255, 255, 0), lane_marking[0], lane_marking[1])

    def checkCollisions(self, car):
        car_edges = car.get_edges()
        for i in range(len(self.points)):
            ln = (self.points[i], self.points[(i + 1) % len(self.points)])
            if (ln[0].y == 0 and ln[1].y == 0) or (
                ln[0].y == self.config.RENDER_HEIGHT
                and ln[1].y == self.config.RENDER_HEIGHT
            ):
                continue
            for edge in car_edges:
                if intersect(*ln, *edge):
                    return True
        return False


class Road:
    def __init__(self, config, points):
        self.config = config
        self.points = points
        self.lane_markings = []

    def add_lane_markings(self, lane_marking):
        self.lane_markings.append(lane_marking)

    def draw(self, screen):
        pygame.draw.polygon(screen, (0, 0, 0), self.points, 10)
        pygame.gfxdraw.filled_polygon(screen, self.points, (60, 60, 60))
        for lane_marking in self.lane_markings:
            draw_line_dashed(screen, (255, 255, 0), lane_marking[0], lane_marking[1])

    def checkCollisions(self, car):
        car_edges = car.get_edges()
        for i in range(len(self.points)):
            ln = (self.points[i], self.points[(i + 1) % len(self.points)])
            if (ln[0].y == 0 and ln[1].y == 0) or (
                ln[0].y == self.config.RENDER_HEIGHT
                and ln[1].y == self.config.RENDER_HEIGHT
            ):
                continue
            for edge in car_edges:
                if intersect(*ln, *edge):
                    return True
        return False


class RoadAsImage:
    def __init__(self, config, road_obj):
        self.config = config
        self.road_obj = road_obj
        self.img = pygame.image.load(road_obj["filename"])
        self.mask = pygame.mask.from_surface(
            pygame.image.load(road_obj["filename"].replace(".png", "_border.png"))
        )
        self.waypoints = (
            list(map(lambda x: Vector2(x[0], x[1]), self.road_obj["waypoints"]))
            if self.road_obj.get("waypoints", None)
            else []
        )

    def draw(self, screen):
        screen.blit(self.img, (0, 0))
        if self.config.DEBUG:
            for wp in self.waypoints:
                pygame.draw.circle(screen, (0, 255, 0), wp, 10)

    def checkCollisions(self, car):
        car_rect = car.get_rotated_image()
        car_mask = pygame.mask.from_surface(car_rect)
        offset = (
            int(car.x - car_rect.get_width() / 2),
            int(car.y - car_rect.get_height() / 2),
        )
        overlap = self.mask.overlap(car_mask, offset)
        return overlap is not None
