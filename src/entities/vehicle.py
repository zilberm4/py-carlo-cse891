import copy

import numpy as np
import pygame
from pygame.math import Vector2

from src.utils import COLORS, intersect, map_to_range

from stable_baselines3 import PPO


class RollingBuffer:
    def __init__(self, size: int = 20, init_speed: float = 0.0):
        self.size = size
        self.cache = [init_speed] * self.size

    def add(self, speed):
        self.cache.append(speed)
        self.cache = self.cache[1:]


class Car:
    def __init__(
        self,
        center: Vector2,
        heading: float,
        movable: bool = True,
        friction: float = 1.0,
        color="blue",
        role="ego",
        mode: str = "ego",
        config=None,
    ):
        self.config = config
        if role != "ego" and role != "adv" and role != "npc":
            raise ValueError("role must be either 'ego' or 'adv'")
        self.mode = mode
        self.center = center  # this is x, y
        self.width = 10
        self.height = 20
        self.role = role
        self.polygonpoints = [
            Vector2(-self.height, -self.width),
            Vector2(self.height, -self.width),
            Vector2(self.height, +self.width),
            Vector2(-self.height, +self.width),
        ]
        self.heading = heading
        self.movable = movable
        self.image = pygame.image.load(f"src/assets/car_{role}.png")
        self.image = pygame.transform.smoothscale(
            self.image, (self.height * 2, self.width * 2)
        )
        self.rotated_image = self.image.copy()

        self.crashed_image = pygame.image.load(f"src/assets/car_crashed.png")
        self.crashed_image = pygame.transform.smoothscale(
            self.crashed_image, (self.height * 2, self.width * 2)
        )
        self.crashed = False

        self.color = COLORS.get(color, (0, 0, 0))
        self.collidable = True
        self.size = Vector2(20, 10)
        self.collision_history = []
        self.finished = False
        if movable:
            self.friction = friction
            self.velocity = Vector2(0, 0)  # this is xp, yp
            self.acceleration = 0  # this is vp (or speedp)
            self.angular_velocity = 0  # this is headingp
            self.inputSteering = 0
            self.inputAcceleration = 0
            self.max_speed = np.inf
            self.min_speed = 0

        self.speed_history = RollingBuffer(size=100, init_speed=self.speed)

    @property
    def speed(self) -> float:
        return (
            np.linalg.norm(np.array([self.velocity.x, self.velocity.y]))
            if self.movable
            else 0
        )

    @property
    def state(self) -> np.ndarray:
        if self.finished:
            return "finished"
        elif self.crashed or (self.speed < 1e-2 and not self.finished):
            return "crashed"
        return "running"

    def set_control(self, inputAcceleration: float, inputSteering: float):
        inputAcceleration = np.tanh(inputAcceleration)
        inputSteering = np.tanh(inputSteering)
        self.inputAcceleration = map_to_range(
            inputAcceleration, -1, 1, self.config.THROTTLE_MIN, self.config.THROTTLE_MAX
        )
        self.inputSteering = map_to_range(
            inputSteering, -1, 1, self.config.STEER_MIN, self.config.STEER_MAX
        )
        if self.config.DEBUG:
            print(
                f"[{self.role}] - inputAcceleration: {self.inputAcceleration} - inputSteering: {self.inputSteering}"
            )

    @property
    def rear_dist(
        self,
    ) -> (
        float
    ):  # distance between the rear wheels and the center of mass. This is needed to implement the kinematic bicycle model dynamics
        return np.maximum(self.size.x, self.size.y) / 2.0

    def tick(self, dt: float, state=None):
        if self.movable:
            speed = self.speed
            self.speed_history.add(speed)
            heading = self.heading

            # Kinematic bicycle model dynamics based on
            # "Kinematic and Dynamic Vehicle Models for Autonomous Driving Control Design" by
            # Jason Kong, Mark Pfeiffer, Georg Schildbach, Francesco Borrelli
            lr = self.rear_dist
            lf = lr  # we assume the center of mass is the same as the geometric center of the entity
            beta = np.arctan(lr / (lf + lr) * np.tan(self.inputSteering))

            new_angular_velocity = (
                speed * self.inputSteering
            )  # this is not needed and used for this model, but let's keep it for consistency (and to avoid if-else statements)
            new_acceleration = 2 * self.inputAcceleration - self.friction
            new_speed = np.clip(
                speed + new_acceleration * dt, self.min_speed, self.max_speed
            )
            new_heading = heading + ((speed + new_speed) / lr) * np.sin(beta) * dt / 2.0
            angle = (heading + new_heading) / 2.0 + beta
            new_center = (
                self.center
                + (speed + new_speed) * Vector2(np.cos(angle), np.sin(angle)) * dt / 2.0
            )
            new_velocity = Vector2(
                new_speed * np.cos(new_heading), new_speed * np.sin(new_heading)
            )

            self.center = new_center
            self.heading = np.mod(
                new_heading, 2 * np.pi
            )  # wrap the heading angle between 0 and +2pi
            self.velocity = new_velocity
            self.acceleration = new_acceleration
            self.angular_velocity = new_angular_velocity

    def get_updated_polypoints(self):
        A = np.array(
            [
                [np.cos(self.heading), -np.sin(self.heading), self.center.x],
                [np.sin(self.heading), np.cos(self.heading), self.center.y],
                [0, 0, 1],
            ]
        )
        new_polygonpoints = np.array(
            [np.append([p.x, p.y], 1) for p in self.polygonpoints]
        )
        rotated_polygonpoints = np.array([A.dot(p) for p in new_polygonpoints])[:, :2]
        return [Vector2(p[0], p[1]) for p in rotated_polygonpoints]

    def get_rotated_image(self):
        angle_in_degrees = -np.degrees(self.heading)
        self.rotated_image = pygame.transform.rotozoom(
            self.image if not self.crashed else self.crashed_image, angle_in_degrees, 1
        )
        return self.rotated_image

    def draw(self, screen):
        if self.config.SIMPLE_RENDER:
            rotated_polygonpoints = self.get_updated_polypoints()
            pygame.gfxdraw.filled_polygon(
                screen,
                rotated_polygonpoints,
                self.color if not self.crashed else COLORS["red"],
            )
            pygame.gfxdraw.aapolygon(screen, rotated_polygonpoints, COLORS["black"])
        else:
            rotated_image = self.get_rotated_image()
            image_rect = rotated_image.get_rect(
                center=(int(round(self.center.x)), int(round(self.center.y)))
            )
            screen.blit(rotated_image, image_rect)

    def get_edges(self):
        rpp = self.get_updated_polypoints()
        return [(rpp[i], rpp[(i + 1) % len(rpp)]) for i in range(len(rpp))]

    def collides(self, other):
        if self.finished or other.finished:
            return False

        rpp_self = self.get_updated_polypoints()
        rpp_other = other.get_updated_polypoints()
        lines_self = [
            (rpp_self[i], rpp_self[(i + 1) % len(rpp_self)])
            for i in range(len(rpp_self))
        ]
        lines_other = [
            (rpp_other[i], rpp_other[(i + 1) % len(rpp_other)])
            for i in range(len(rpp_other))
        ]
        for line_self in lines_self:
            for line_other in lines_other:
                if intersect(*line_self, *line_other):
                    return True
        return False

    def copy(self):
        return copy.deepcopy(self)

    @property
    def x(self):
        return self.center.x

    @property
    def y(self):
        return self.center.y

    @property
    def xp(self):
        return self.velocity.x

    @property
    def yp(self):
        return self.velocity.y


# import PIL
import PIL.Image as Image


class PPOCar(Car):
    def __init__(
        self,
        center: Vector2,
        heading: float,
        model: str,
        movable: bool = True,
        friction: float = 1.0,
        color="blue",
        role="ego",
        mode: str = "ppo",
        config=None,
    ):
        super().__init__(center, heading, movable, friction, color, role, mode, config)
        self.model = PPO.load(model)

    def tick(self, dt, state):
        # add 1 dimension to state
        state = np.expand_dims(state, axis=0).astype(np.float32)
        action, _states = self.model.predict(state, deterministic=False)
        action = action[0]
        self.set_control(action[0], action[1])
        super().tick(self.config.DT)


# hardcoded autonomous car
class AutoCar(Car):
    def __init__(
        self,
        center: Vector2,
        heading: float,
        movable: bool = True,
        friction: float = 1.0,
        color="blue",
        role="ego",
        mode: str = "auto",
        config=None,
    ):
        super().__init__(center, heading, movable, friction, color, role, mode, config)

    def tick(self, dt, state=None):
        if len(self.collision_history) > 0 and not self.finished:
            sp = self.speed
            self.center -= self.velocity / sp * (5 * self.config.DT)
        if (
            self.role == "adv" and abs(self.center.y - 150) < 10
        ):  # TODO: hacky fix to allow npc to finish track
            self.inputSteering = -0.075

        super().tick(self.config.DT)


class WaypointCar(Car):
    def __init__(
        self,
        center: Vector2,
        heading: float,
        waypoints: list,
        movable: bool = True,
        friction: float = 1.0,
        color="blue",
        role="ego",
        mode: str = "waypoint",
        config=None,
    ):
        super().__init__(center, heading, movable, friction, color, role, mode, config)
        self.waypoints = waypoints
        self.curr_waypoint = 0

    def tick(self, dt, state=None):
        if self.finished:
            return

        # get closest waypoint
        self.friction = 0.0
        self.inputSteering = 0
        self.inputAcceleration = 0
        closest_waypoint = self.waypoints[self.curr_waypoint]
        if self.center.distance_to(closest_waypoint) < 30:
            self.curr_waypoint += 1
            if self.curr_waypoint >= len(self.waypoints):
                self.finished = True
                return
            closest_waypoint = self.waypoints[self.curr_waypoint % len(self.waypoints)]

        # get vec curr, vec to wp
        v1 = self.velocity
        v2 = closest_waypoint - self.center
        # noramlize
        v1 = v1 / (np.linalg.norm(v1) + 1e-8)
        v2 = v2 / (np.linalg.norm(v2) + 1e-8)
        # calc angle
        theta = np.arccos(np.clip(np.dot(v1, v2), -1.0, 1.0))

        # update steering
        # if abs(theta) > 0.1:
        if np.cross(v1, v2) > 0:
            self.inputSteering = (0.3) * theta
        else:
            self.inputSteering = (-0.3) * theta

        super().tick(self.config.DT)


class DistractedCar(Car):
    def __init__(
        self,
        center: Vector2,
        heading: float,
        model: str,
        movable: bool = True,
        friction: float = 1.0,
        color="blue",
        role="ego",
        mode: str = "distracted",
        config=None,
    ):
        super().__init__(center, heading, movable, friction, color, role, mode, config)
        self.distract_rate = 0.0075
        self.distract_duration = 100
        self.distract_counter = 0
        self.is_distracted = False
        self.counter = 0
        self.model = PPO.load(model)

    def set_control(self, inputAcceleration: float, inputSteering: float):
        if self.is_distracted:
            return
        return super().set_control(inputAcceleration, inputSteering)

    def tick(self, dt, state=None):
        if not self.is_distracted:
            if np.random.random() < self.distract_rate:
                self.is_distracted = True

        if self.is_distracted:
            self.distract_counter += 1
            if self.distract_counter > self.distract_duration:
                self.is_distracted = False
                self.distract_counter = 0

        if self.config.DEBUG:
            print(f"{self.counter} Distract state: {self.is_distracted}")

        # state = np.expand_dims(state, axis=0).astype(np.float32)
        # action, _states = self.model.predict(state, deterministic=False)
        # action = action[0]
        # self.set_control(action[0], action[1])
        super().tick(self.config.DT)


class AdaptiveCruiseCar(Car):
    def __init__(
        self,
        center: Vector2,
        heading: float,
        movable: bool = True,
        friction: float = 0.0,
        color="blue",
        role="ego",
        mode="acc",
        acc_type: str = "old",
        config=None,
    ):
        super().__init__(center, heading, movable, friction, color, role, mode, config)
        self.other_vehicles = None
        self.K_p = 0.15
        self.K_d = 0.0
        self.K_i = 0.0003
        self.prev_setpoint = 0.0
        self.integral_setpoint = 0.0
        self.maintaining_distance = False
        self.target_speed = 40
        self.acc_type = acc_type
        self.hard_break_counter = 0

    def tick_no_gap(self, dt, state=None):
        if self.other_vehicles is None:
            return

        closest_vehicle = None
        min_dist = np.inf
        for vehicle in self.other_vehicles:
            xdist = abs(self.x - vehicle.x)
            ydist = vehicle.y - self.y

            # not in front, continue
            if ydist > 10:
                continue
            # not in our lane, continue
            if xdist > 25:
                continue
            dist = self.center.distance_to(vehicle.center)
            if dist <= min_dist:
                min_dist = dist
                closest_vehicle = vehicle

        gap = 160
        gap_dampen = 0.625
        speed = self.speed
        dist_to_car = self.center.distance_to(closest_vehicle.center)
        if dist_to_car < gap_dampen * gap:
            self.inputAcceleration = 0.0
            self.velocity = closest_vehicle.velocity
            if self.config.DEBUG:
                print("ACC: hard brake!")
            super().tick(self.config.DT)
            return

        # signal
        delta_distance = dist_to_car - 2 * gap - speed**2 / (2 * 15)

        # if other too slow, keep safe distance
        if delta_distance < 0:
            self.maintaining_distance = True
        elif speed >= self.target_speed:  # should slow down
            self.maintaining_distance = False
        if self.maintaining_distance:
            # ovveride if we are too close
            set_point = delta_distance
        else:
            # else go to cruise speed
            set_point = self.target_speed - speed

        # | https://en.wikipedia.org/wiki/PID_controller |
        # pid = Kp*error + Kd*(dt(err)) + Ki*integral(error)
        control = (
            (self.K_p * set_point)
            + (self.K_d * (set_point - self.prev_setpoint))
            + (self.K_i * self.integral_setpoint)
        )

        control = np.clip(control, -1, 1)
        print(f"control: {control} | setpoint {set_point} | speed {speed}")
        throttle_damp = 0.25
        if control >= 0:
            self.inputAcceleration = control * throttle_damp
        if control < 0:
            self.inputAcceleration = control * throttle_damp

        self.prev_setpoint = set_point
        self.integral_setpoint = self.integral_setpoint + set_point
        super().tick(self.config.DT)

    def tick_gap(self, dt, state=None):
        if self.other_vehicles is None:
            return

        closest_vehicle = None
        min_dist = np.inf
        for vehicle in self.other_vehicles:
            xdist = abs(self.x - vehicle.x)
            ydist = vehicle.y - self.y

            # not in front, continue
            if ydist > 10:
                continue
            # not in our lane, continue
            if xdist > 25:
                continue

            # normal_vec = Vector2(0,-1)
            # vec_to_vehicle = (vehicle.center - self.center).normalize()
            # ang = normal_vec.angle_to(vec_to_vehicle)
            dist = self.center.distance_to(vehicle.center)
            if dist <= min_dist:
                min_dist = dist
                closest_vehicle = vehicle

        gap = 180
        gap_dampen = 0.625
        speed = self.speed
        dist_to_car = self.center.distance_to(closest_vehicle.center)
        if dist_to_car < gap_dampen * gap:
            self.hard_break_counter += 1
            self.inputAcceleration = 0.0
            self.velocity = closest_vehicle.velocity * 0.75
            if self.config.DEBUG:
                print("ACC: hard brake!")

            super().tick(self.config.DT)
            return

        # signal
        delta_distance = dist_to_car - 2 * gap - speed**2 / (2 * 15)

        # if other too slow, keep safe distance
        if delta_distance < 0:
            self.maintaining_distance = True
        elif speed >= self.target_speed:  # should slow down
            self.maintaining_distance = False
        if self.maintaining_distance:
            # ovveride if we are too close
            set_point = delta_distance
        else:
            # else go to cruise speed
            set_point = closest_vehicle.speed - speed

        # | https://en.wikipedia.org/wiki/PID_controller |
        # pid = Kp*error + Kd*(dt(err)) + Ki*integral(error)
        control = (
            (self.K_p * set_point)
            + (self.K_d * (set_point - self.prev_setpoint))
            + (self.K_i * self.integral_setpoint)
        )

        control = np.clip(control, -1, 1)
        # print(f"control: {control} | setpoint {set_point} | speed {speed} | acc {self.inputAcceleration}")
        throttle_damp = 0.25
        if control >= 0:
            self.inputAcceleration = control * throttle_damp
        if control < 0:
            self.inputAcceleration = 2.0 * control * throttle_damp

        self.prev_setpoint = set_point
        self.integral_setpoint = self.integral_setpoint + set_point

        super().tick(self.config.DT)

    def tick(self, dt: float, state=None):
        if self.acc_type == "old":
            self.tick_gap(dt, state)
        elif self.acc_type == "new":
            self.tick_no_gap(dt, state)


class NPCCar(Car):
    def __init__(
        self,
        center: Vector2,
        heading: float,
        movable: bool = True,
        friction: float = 0.0,
        color="green",
        role="npc",
        mode: str = "npc",
        init_speed=Vector2(20, 0),
        config=None,
    ):
        self.init_speed = init_speed
        super().__init__(center, heading, movable, friction, color, role, mode, config)

    def tick(self, dt, state=None):
        self.velocity = self.init_speed
        self.finished = False
        self.inputSteering = 0.0
        self.inputAcceleration = 0.1
        # print(f'v: {self.velocity} | xy: {self.center}')
        super().tick(self.config.DT)
