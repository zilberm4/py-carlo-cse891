import gymnasium as gym
import numpy as np
import sys

sys.modules["gym"] = gym
from src.renderer import Renderer
from src.sim.simulation import Simulation


class ReinforcementEnvironmentWrapper(gym.Env):
    """
    Reinforcement Learning Environment for Carla
    """

    def __init__(self, config):
        self.config = config
        self.img_width = self.config.RENDER_WIDTH
        self.img_height = self.config.RENDER_HEIGHT
        self.action_space = gym.spaces.box.Box(
            np.array([-1, -1]).astype(np.float32),
            np.array([1, 1]).astype(np.float32),
        )
        self.observation_space = gym.spaces.Box(
            low=0,
            high=255,
            shape=(
                self.config.STACKED_BUFFER_SIZE
                if self.config.STACKED_BUFFER_SIZE != 1
                else 3,
                self.config.OBSERVATION_SPACE_WIDTH,
                self.config.OBSERVATION_SPACE_HEIGHT,
            ),
            dtype=np.uint8,
        )
        self.num_envs = 1

        self.simulation = Simulation(config)
        self.renderer = Renderer(config)

    def reset(self):
        self.simulation.reset()
        self.renderer.reset()
        return self.renderer.tick(self.simulation)

    def step(self, action):
        _, reward, done, info = self.simulation.step(action)
        state = self.renderer.tick(self.simulation)
        return state, reward, done, info

    def close(self):
        self.renderer.close()
        print("[STATUS] Exiting...")
