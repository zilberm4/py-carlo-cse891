from stable_baselines3 import PPO
from stable_baselines3 import SAC
from stable_baselines3.common.callbacks import CheckpointCallback, EveryNTimesteps
from stable_baselines3.common.vec_env import (
    VecFrameStack,
    DummyVecEnv,
    VecMonitor,
    VecNormalize,
)
import gymnasium as gym
from src.environment import ReinforcementEnvironmentWrapper

from stable_baselines3.common.logger import configure
from stable_baselines3.common.evaluation import evaluate_policy

from stable_baselines3.common.callbacks import BaseCallback, EvalCallback
import pandas as pd
import matplotlib.pyplot as plt

from src.utils import preload, rename_to_mode
import os
import sys

from src.plot import plot_boxcharts


class SpeedEvalCallback(BaseCallback):
    def __init__(self, config, n_evals=10, verbose=0):
        super(SpeedEvalCallback, self).__init__(verbose)
        self.config = config
        self.per_episode_speed_cache = []
        self.avg_speed_cache = []
        self.n_evals = n_evals

    def _plot_avg_speed(self, data):
        plt.boxplot(
            [data["avg_ego_speed"], data["avg_adv_speed"]], labels=["ego", "adv"]
        )
        plt.savefig("speed.png")

    def _on_step(self, _locals, _globals) -> bool:
        info_dict = _locals["info"]
        self.per_episode_speed_cache.append(
            {
                "ego_speed": info_dict["ego_speed"],
                "adv_speed": info_dict["adv_speed"],
            }
        )
        done = _locals["done"]
        if done:
            data = pd.DataFrame(self.per_episode_speed_cache)
            avg_ego_speed = data["ego_speed"].mean()
            avg_adv_speed = data["adv_speed"].mean()
            self.per_episode_speed_cache = []
            # self._plot_avg_speed(data)
            self.avg_speed_cache.append(
                {
                    "avg_ego_speed": avg_ego_speed,
                    "avg_adv_speed": avg_adv_speed,
                    "ego_state": info_dict["ego_state"],
                    "adv_state": info_dict["adv_state"],
                }
            )
            print(f"[INFO] Episode {len(self.avg_speed_cache)}/{self.n_evals}")

        if len(self.avg_speed_cache) == self.n_evals:
            data = pd.DataFrame(self.avg_speed_cache)
            with open(f"{self.config.EXPERIMENT_DIR}/avg_speed.csv", "w") as f:
                data.to_csv(f, index=False)
            # self._plot_avg_speed(data)
            self.avg_speed_cache = []
            return False

        return True

    __call__ = _on_step


def generate_env(config, params: dict, eval_mode=False):
    env = ReinforcementEnvironmentWrapper(config)
    if not config.DISPLAY_ENABLED:
        env.renderer.disable_display()
    env = DummyVecEnv([lambda: env])
    env = VecMonitor(env)
    env = VecNormalize(env, norm_obs=False, norm_reward=True)

    if params.stacked_buffer_size > 1:
        env = VecFrameStack(env, n_stack=params.stacked_buffer_size)

    return env


def run_eval(config, params, ckpt_path, mode="ego", n_evals=15):
    config.SCENARIO_TYPE = mode
    config.SCENARIO_FILE = f"src/settings/{params.scene}/{mode}.yaml"
    env = generate_env(config, params)
    print(f"Loading model from {ckpt_path}...")
    model = PPO.load(ckpt_path, env=env)
    new_logger = configure(config.EXPERIMENT_DIR, ["stdout", "csv"])
    model.set_logger(new_logger)
    cc = SpeedEvalCallback(config, n_evals=n_evals)
    mean_reward, std_reward = evaluate_policy(
        model, env, n_eval_episodes=n_evals, callback=cc
    )
    env.close()
    print(f"mean_reward ({mode}):{mean_reward:.2f} +/- {std_reward:.2f}")


def evaluate(config, params: dict):
    N_EVALS = params.n_evals
    path_prog = f"{config.EXPERIMENT_DIR}/progress.csv"
    path_speed = f"{config.EXPERIMENT_DIR}/avg_speed.csv"

    vehicles = preload(config.SCENARIO_FILE)["vehicles"]
    ego_path = vehicles[0]["model"]
    adv_path = vehicles[1]["model"]

    run_eval(config, params, ego_path, mode="ego", n_evals=N_EVALS)
    rename_to_mode(path_prog, "ego")
    rename_to_mode(path_speed, "ego")

    mode = "adv"
    config.SCENARIO_TYPE = mode
    config.SCENARIO_FILE = f"src/settings/{config.SCENE}/{mode}.yaml"
    vehicles = preload(config.SCENARIO_FILE)["vehicles"]
    ego_path = vehicles[0]["model"]
    adv_path = vehicles[1]["model"]
    run_eval(config, params, adv_path, mode="adv", n_evals=N_EVALS)
    rename_to_mode(path_prog, "adv")
    rename_to_mode(path_speed, "adv")

    mode = "ret"
    config.SCENARIO_TYPE = mode
    config.SCENARIO_FILE = f"src/settings/{config.SCENE}/{mode}.yaml"
    vehicles = preload(config.SCENARIO_FILE)["vehicles"]
    ego_path = vehicles[0]["model"]
    adv_path = vehicles[1]["model"]
    run_eval(config, params, ego_path, mode="ret", n_evals=N_EVALS)
    rename_to_mode(path_prog, "ret")
    rename_to_mode(path_speed, "ret")

    # plot_boxcharts([f'{config.EXPERIMENT_DIR}/adv_avg_speed.csv',f'{config.EXPERIMENT_DIR}/ret_avg_speed.csv'], ['adv', 'ret'])
    plot_boxcharts(
        [
            f"{config.EXPERIMENT_DIR}/ego_avg_speed.csv",
            f"{config.EXPERIMENT_DIR}/adv_avg_speed.csv",
            f"{config.EXPERIMENT_DIR}/ret_avg_speed.csv",
        ],
        ["baseline", "adv", "ret"],
    )
