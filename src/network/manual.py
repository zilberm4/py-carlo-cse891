from src.environment import ReinforcementEnvironmentWrapper
from src.renderer import Renderer
from src.sim.simulation import Simulation
import pygame
from pygame import Vector2
from src.settings import Config
from src.utils import COLORS
import sys
import os


def play():
    config = Config()
    # env = ReinforcementEnvironmentWrapper()
    # # run the simulation
    # env.reset()
    # while 1:
    #     _, _, done, _ = env.step((0, 0))
    #     if done:
    #         env.reset()
    # pygame.init()
    if config.RECORDER_ENABLED:
        os.makedirs(config.EXPERIMENT_DIR, exist_ok=True)

    pygame.display.set_caption("Reinforcement Learning Environment")
    screen = pygame.display.set_mode((config.RENDER_WIDTH, config.RENDER_HEIGHT))
    renderer = Renderer()
    clock = pygame.time.Clock()
    simulation = Simulation()
    simulation.dt = 0.1

    while 1:
        event_occ = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                renderer.close()
            if event.type == pygame.KEYDOWN:
                event_occ = True
                if event.key == pygame.K_LEFT:
                    simulation.step((0, -1))
                if event.key == pygame.K_RIGHT:
                    simulation.step((0, 1))
                if event.key == pygame.K_UP:
                    simulation.step((3, 0))
                if event.key == pygame.K_DOWN:
                    simulation.step((-3, 0))
                if event.key == pygame.K_r:
                    simulation.reset()
        if not event_occ:
            simulation.tick()

        mx, my = pygame.mouse.get_pos()
        # screen.fill((127, 127, 127))
        # for entity in simulation.roads:
        #     entity.draw(screen)

        # for entity in simulation.vehicles:
        #     if len(entity.collision_history) > 0:
        #         entity.color = COLORS["red"]
        #         simulation.reset()
        #         # entity.velocity = Vector2(0, 0)
        #     entity.draw(screen)

        # print(simulation.ego.speed)
        renderer.tick(simulation)
