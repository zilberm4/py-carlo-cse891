from stable_baselines3 import PPO
from stable_baselines3 import SAC
from stable_baselines3.common.callbacks import CheckpointCallback, EveryNTimesteps
from stable_baselines3.common.vec_env import (
    VecFrameStack,
    DummyVecEnv,
    VecMonitor,
    VecNormalize,
)
import gymnasium as gym
from src.environment import ReinforcementEnvironmentWrapper

from stable_baselines3.common.logger import configure
from stable_baselines3.common.evaluation import evaluate_policy

from stable_baselines3.common.callbacks import BaseCallback
import pandas as pd
import matplotlib.pyplot as plt

from src.utils import preload


class SpeedEvalCallback(BaseCallback):
    def __init__(self, config, n_evals=10, verbose=0):
        super(SpeedEvalCallback, self).__init__(verbose)
        self.config = config
        self.per_episode_speed_cache = []
        self.avg_speed_cache = []
        self.n_evals = n_evals

    def _plot_avg_speed(self, data):
        with open(f"{self.config.EXPERIMENT_DIR}/avg_speed.csv", "w") as f:
            data.to_csv(f, index=False)

        plt.boxplot(
            [data["avg_ego_speed"], data["avg_adv_speed"]], labels=["ego", "adv"]
        )
        plt.savefig("speed.png")

    def _on_step(self, _locals, _globals) -> bool:
        try:
            info_dict = _locals["info"]
            self.per_episode_speed_cache.append(
                {
                    "ego_speed": info_dict["ego_speed"],
                    "adv_speed": info_dict["adv_speed"],
                }
            )
            done = _locals["done"]
            if done:
                data = pd.DataFrame(self.per_episode_speed_cache)
                avg_ego_speed = data["ego_speed"].mean()
                avg_adv_speed = data["adv_speed"].mean()
                self.per_episode_speed_cache = []
                # self._plot_avg_speed(data)
                self.avg_speed_cache.append(
                    {
                        "avg_ego_speed": avg_ego_speed,
                        "avg_adv_speed": avg_adv_speed,
                    }
                )

            if len(self.avg_speed_cache) == self.n_evals:
                data = pd.DataFrame(self.avg_speed_cache)
                self._plot_avg_speed(data)
                self.avg_speed_cache = []
                return False

            return True
        except Exception as e:
            pass

    __call__ = _on_step


def test(config, params):
    N_EVALS = params.n_evals
    ckpt_path = params.checkpoint
    env = ReinforcementEnvironmentWrapper(config)
    env = DummyVecEnv([lambda: env])
    env = VecMonitor(env)
    env = VecNormalize(env, norm_obs=False, norm_reward=True)
    if config.STACKED_BUFFER_SIZE > 1:
        env = VecFrameStack(env, n_stack=config.STACKED_BUFFER_SIZE)

    if ckpt_path is None:
        data = preload(config.SCENARIO_FILE)["vehicles"]
        if config.SCENARIO_TYPE != "adv":
            ckpt_path = data[0]["model"]
        else:
            ckpt_path = data[1]["model"]

    print(f"Loading model from {ckpt_path}...")
    model = PPO.load(ckpt_path, env=env)

    new_logger = configure(config.EXPERIMENT_DIR, ["stdout", "csv"])
    model.set_logger(new_logger)
    cc = SpeedEvalCallback(config, n_evals=N_EVALS)

    # checkpoint_callback = CheckpointCallback(
    #     save_freq=int(2.5e5),
    #     save_path=config.EXPERIMENT_DIR,
    #     name_prefix="rl_model_sb3_SAC",
    # )
    # model.learn(3e6, callback=checkpoint_callback)
    # model.save("cnn_ppo")
    mean_reward, std_reward = evaluate_policy(
        model, env, n_eval_episodes=N_EVALS, callback=cc
    )
    env.close()
    print(f"mean_reward:{mean_reward:.2f} +/- {std_reward:.2f}")
