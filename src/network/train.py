from stable_baselines3 import PPO
from stable_baselines3 import SAC
from stable_baselines3.common.callbacks import (
    CheckpointCallback,
    EveryNTimesteps,
    EvalCallback,
    CallbackList,
)
from stable_baselines3.common.vec_env import (
    VecFrameStack,
    DummyVecEnv,
    VecMonitor,
    VecNormalize,
    SubprocVecEnv,
)
import gymnasium as gym
from src.environment import ReinforcementEnvironmentWrapper
from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.logger import configure


def dump_model_config(params, path):
    # save argparser params
    with open(path, "w") as f:
        for key, value in vars(params).items():
            print(f"{key}: {value}", file=f)


def generate_env(config, params: dict, eval_mode=False):
    env = ReinforcementEnvironmentWrapper(config)
    if not config.DISPLAY_ENABLED:
        env.renderer.disable_display()
    env = DummyVecEnv([lambda: env])
    env = VecMonitor(env)
    env = VecNormalize(env, norm_obs=False, norm_reward=True)

    if params.stacked_buffer_size > 1:
        env = VecFrameStack(env, n_stack=params.stacked_buffer_size)

    return env


def set_progress_bar():
    try:
        import tqdm
        import rich

        return True
    except ImportError:
        return False


def train(config, params: dict):
    dump_model_config(params, f"{config.EXPERIMENT_DIR}/params.txt")
    env = generate_env(config, params)

    if params.checkpoint:
        print(f"[INFO] Resuming training from {params.checkpoint}")
        model = PPO.load(params.checkpoint, env=env)
    else:
        model = PPO(
            "CnnPolicy",
            env,
            verbose=1,
            batch_size=params.batch_size,
            n_steps=params.n_steps,
            n_epochs=params.n_epochs,
            gamma=params.gamma,
            gae_lambda=params.gae_lambda,
            clip_range=params.clip_range,
            ent_coef=params.ent_coef,
            vf_coef=params.vf_coef,
            learning_rate=params.learning_rate,
        )

    logging_fmt = ["stdout", "csv"]
    if params.tensorboard:
        logging_fmt.append("tensorboard")
    print(f"[INFO] Logging to {logging_fmt}")
    new_logger = configure(config.EXPERIMENT_DIR, logging_fmt)
    model.set_logger(new_logger)

    checkpoint_callback = CheckpointCallback(
        save_freq=params.save_freq,
        save_path=config.EXPERIMENT_DIR,
        name_prefix=f"{params.scene}_{params.scenario}_model",
    )

    # eval_env = generate_env(params, eval_mode=True)
    # eval_callback = EvalCallback(eval_env, best_model_save_path=config.EXPERIMENT_DIR,
    #                          log_path=config.EXPERIMENT_DIR, eval_freq=1e3,
    #                          deterministic=True, render=False)

    # cb_list = CallbackList([checkpoint_callback, eval_callback])

    model.learn(params.total_timesteps, callback=checkpoint_callback)
    model.save("cnn_ppo")
