import datetime
import json
import os
import uuid
import yaml
import sys

sys.path.append(".")

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pygame

import seaborn as sns
import argparse
from src.utils import get_unique_filename, ensure_dir_structure


def plot_multiple_sb_data(in_date_filenames, labels):
    if len(in_date_filenames) != len(labels):
        raise ValueError("Number of filenames and labels must be equal")
    plot_single_trial = all([label is None for label in labels])

    fig, ax = plt.subplots(2, 2, figsize=(12, 8))
    dest_dir = (
        os.path.dirname(in_date_filenames[0]) if plot_single_trial else "data/plots"
    )
    exp_name = os.path.basename(os.path.dirname(in_date_filenames[0]))
    ensure_dir_structure(dest_dir)
    pref = "single_trial_" if plot_single_trial else "multi_trial"
    suff = "" if plot_single_trial else f"{get_unique_filename()}"
    fname = os.path.join(dest_dir, f"{pref}_stats_{suff}.png")

    # Create the grid of plots
    fig, ax = plt.subplots(2, 2, figsize=(12, 8))
    for infile, label in zip(in_date_filenames, labels):
        data = pd.read_csv(infile)
        # data = data[data['time/total_timesteps'] < 5e5]
        # divide

        roll_avg = 10
        ax[0, 0].scatter(
            data["time/total_timesteps"], data["rollout/ep_rew_mean"], s=1, alpha=0.25
        )
        ax[0, 1].scatter(
            data["time/total_timesteps"], data["train/loss"], s=1, alpha=0.25
        )
        ax[1, 0].scatter(
            data["time/total_timesteps"], data["train/value_loss"], s=1, alpha=0.25
        )
        ax[1, 1].scatter(
            data["time/total_timesteps"],
            data["train/explained_variance"],
            s=1,
            alpha=0.25,
        )

        ax[0, 0].plot(
            data["time/total_timesteps"],
            data["rollout/ep_rew_mean"].rolling(window=roll_avg).mean(),
            label=label,
        )
        ax[0, 1].plot(
            data["time/total_timesteps"],
            data["train/loss"].rolling(window=roll_avg).mean(),
            label=label,
        )
        ax[1, 0].plot(
            data["time/total_timesteps"],
            data["train/value_loss"].rolling(window=roll_avg).mean(),
            label=label,
        )
        ax[1, 1].plot(
            data["time/total_timesteps"],
            data["train/explained_variance"].rolling(window=roll_avg).mean(),
            label=label,
        )

    if not plot_single_trial:
        ax[0, 0].legend()
        ax[0, 1].legend()
        ax[1, 0].legend()
        ax[1, 1].legend()

    ax[0, 0].set_title(r"Rewards  $\uparrow$")
    ax[0, 0].set_xlabel("Timestep")
    ax[0, 0].set_ylabel("Mean Reward")
    ax[0, 1].set_title(r"Loss $\downarrow$")
    ax[0, 1].set_xlabel("Timestep")
    ax[0, 1].set_ylabel("Loss")
    # ax[0, 1].set_ylim([-0.01, 0.5])
    ax[1, 0].set_title(r"Value Loss $\downarrow$")
    ax[1, 0].set_xlabel("Timestep")
    ax[1, 0].set_ylabel("Value Loss")
    # ax[1, 0].set_ylim([0, 0.5])
    ax[1, 1].set_title(r"Explained Variance $\uparrow$")
    ax[1, 1].set_xlabel("Timestep")
    ax[1, 1].set_ylabel("Explained Variance")
    plt.tight_layout()
    plt.savefig(fname)


import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import PathPatch


def adjust_box_widths(g, fac):
    for ax in g.axes:
        for c in ax.get_children():
            if isinstance(c, PathPatch):
                p = c.get_path()
                verts = p.vertices
                verts_sub = verts[:-1]
                xmin = np.min(verts_sub[:, 0])
                xmax = np.max(verts_sub[:, 0])
                xmid = 0.5 * (xmin + xmax)
                xhalf = 0.5 * (xmax - xmin)
                xmin_new = xmid - fac * xhalf
                xmax_new = xmid + fac * xhalf
                verts_sub[verts_sub[:, 0] == xmin, 0] = xmin_new
                verts_sub[verts_sub[:, 0] == xmax, 0] = xmax_new
                for l in ax.lines:
                    if np.all(l.get_xdata() == [xmin, xmax]):
                        l.set_xdata([xmin_new, xmax_new])

import pathlib
def plot_boxcharts(file_paths: list, scenario_labels: list = None, plot_dir: str = "data/plots"):
    if not scenario_labels:
        scenario_labels = ["Baseline", "Adversarial"]
        if len(file_paths) == 3:
            scenario_labels.append("Retrained")
    data_frames = []

    try:
        PREFIX = pathlib.Path(file_paths[0]).parents[1].name.replace("eval","")
    except:
        PREFIX = ""
    for idx, file_path in enumerate(file_paths):
        data = pd.read_csv(file_path)
        data["scenario"] = scenario_labels[idx]
        data_frames.append(data)

    combined_data = pd.concat(data_frames)
    reshaped_data = pd.melt(
        combined_data,
        id_vars=["scenario"],
        value_vars=["avg_ego_speed", "avg_adv_speed"],
        var_name="agent",
        value_name="average_speed",
    )
    reshaped_data = reshaped_data.loc[
        ~(
            (reshaped_data["scenario"] == "Baseline")
            & (reshaped_data["agent"] == "avg_adv_speed")
        )
    ]

    palette = sns.color_palette("Paired")[1:]
    sns.set_palette(palette)

    fig = plt.figure()
    ax = sns.boxplot(
        x="scenario",
        y="average_speed",
        showfliers=False,
        hue="agent",
        data=reshaped_data,
        showmeans=True,
        meanprops={
            "marker": "o",
            "markerfacecolor": "white",
            "markeredgecolor": "blue",
        },
    )
    adjust_box_widths(fig, 0.9)
    plt.title(f'Average Speed Comparison in \n {", ".join(scenario_labels)}')
    plt.xlabel("Scenario")
    plt.ylabel("Average Speed")

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(title="Agent", handles=handles, labels=["Ego", "Non-Ego"])

    ax.set_xticklabels(scenario_labels)
    box_fname = f"{plot_dir}/{PREFIX}_boxplot_{get_unique_filename()}.png"
    plt.savefig(box_fname)

    success_data = []

    for idx, data in enumerate(data_frames):
        ego_fail_adv_success = data[
            (data["ego_state"] == "crashed") & (data["adv_state"] == "finished")
        ]
        ego_states = len(ego_fail_adv_success) / len(data)
        adv_states = data["adv_state"].value_counts().get("crashed", 0) / len(data)
        success_data.append({"scenario": scenario_labels[idx], "ego_state": ego_states})

    combined_success_data = pd.DataFrame(success_data)
    reshaped_success_data = pd.melt(
        combined_success_data,
        id_vars=["scenario"],
        value_vars=["ego_state"],
        var_name="agent",
        value_name="failure_rate",
    )

    plt.figure()
    ax = sns.barplot(x="scenario", y="failure_rate", data=reshaped_success_data)

    for p in ax.patches:
        ax.annotate(
            format(p.get_height(), ".2f"),
            (p.get_x() + p.get_width() / 2.0, p.get_height()),
            ha="center",
            va="center",
            xytext=(0, 10),
            textcoords="offset points",
        )

    bar_fname = f"{plot_dir}/{PREFIX}_barplot_{get_unique_filename()}.png"
    plt.title(f'Average Failure Rate Comparison in\n {", ".join(scenario_labels)}')
    plt.xlabel("Scenario")
    plt.ylabel("Failure Rate")
    ax.set_ylim([0, 1])
    ax.set_xticklabels(scenario_labels)
    ax.legend(title="Agent", handles=handles, labels=["Ego", "Non-Ego"])
    plt.savefig(bar_fname)

    print(f"Plots saved to {box_fname} and {bar_fname}")


if __name__ == "__main__":
    """
    Script to plot multiple graphs at the same time.
    n-args for both parameters must match

    Inputs: --archive-path / -a: list of .csvs. Can be done with */*.csv
            --labels / -l: labels to be used for the graphs. Ordered by csv appearance

    Example usage: python3 src/plot.py -a ./data/sb/exp_-04-15-21-59-48-11b/*.csv ./data/sb/exp_-04-15-21-59-48-d8b/*.csv -l 0.9 0.99

    Example usage: python3 src/plot.py -a ./data/sb/*/*.csv -l 0.9 0.99
        Assumption ./data/sb/*/*.csv produces 2 files only.
    """

    # import argparse

    # # read command-line arguments
    # parser = argparse.ArgumentParser()
    # parser.add_argument("-a", "--archive_path", nargs="+", type=str, required=True)
    # parser.add_argument(
    #     "-l", "--labels", nargs="+", help="Input a list of labels", type=str
    # )

    # args = parser.parse_args()

    # archive_path = [x for x in args.archive_path]
    # labels = [float(item) for item in args.labels] if args.labels else [None]

    # if len(archive_path) != len(labels):
    #     raise Exception(
    #         "Parser failure: archive_path should have same number of arguments as labels"
    #     )

    # plot_multiple_sb_data(['data/train/exp_final_v3-04-19-17-20-13-c5d/progress.csv'], [None])
    # exp_dir = "data/eval_merge/exp_ego_-07-11-12-45-39-0cc/"  #'data/eval_distracted/exp_ego_-07-11-14-11-41-8c8/'
    exp_dir = "data/eval_distracted/exp_ego_-07-13-13-15-59-be7/"  #'data/eval_distracted/exp_ego_-07-11-14-11-41-8c8/'
    plot_boxcharts(
        [
            f"{exp_dir}/ego_avg_speed.csv",
            f"{exp_dir}/adv_avg_speed.csv",
            f"{exp_dir}/ret_avg_speed.csv",
        ],
        ["Baseline", "Non-Ego", "Retrained"],
        "/mnt/c/Users/solbob/Downloads/sdrl_distracted_plots_071323/"
    )

    # 'data/eval_merge/exp_ego_-07-10-16-43-25-8e3/ego_avg_speed.csv',
    # plot_boxcharts(['data/eval_merge/exp_ego_-07-10-16-43-25-8e3/adv_avg_speed.csv', 'data/eval_merge/exp_ego_-07-10-16-43-25-8e3/ret_avg_speed.csv'], ['adv', 'ret'])
