import sys

import numpy as np
import PIL.Image
import pygame
from pygame.math import Vector2

from src.utils import COLORS

import imageio
import os
import pygame
import random


class Recorder:
    def __init__(self, outfile="demo.mp4"):
        self.outfile = outfile
        self.file_count = 0
        self.temp_folder = f".__temp_gif{random.randint(0, 1000000)}"
        os.mkdir(self.temp_folder)

    def click(self, screen):
        fn = f"{self.temp_folder}/{self.file_count}.png"
        pygame.image.save(screen, fn)
        self.file_count += 1

    def save(self):
        files = os.listdir(self.temp_folder)
        files = sorted(files, key=lambda x: int(os.path.splitext(x)[0]))
        # print(files)
        images = []
        for filename in files:
            images.append(imageio.imread(f"./{self.temp_folder}/{filename}"))
        imageio.mimsave(self.outfile, images, fps=60)

    def __del__(self):
        files = os.listdir(self.temp_folder)
        for filename in files:
            os.remove(f"./{self.temp_folder}/{filename}")
        os.rmdir(self.temp_folder)


def generate_text_surface(text, color, size):
    font = pygame.font.Font(None, size)
    text_surface = font.render(text, True, color)
    return text_surface


class Renderer:
    def __init__(self, config):
        self.config = config
        self.screen = None
        self.surf = None
        self.clock = pygame.time.Clock()
        self.display_enabled = self.config.DISPLAY_ENABLED

        if self.config.RECORDER_ENABLED:
            self.recorder = Recorder(outfile=self.config.EXPERIMENT_DIR + "/demo.mp4")

    def event_cb(self):
        events = []
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.close()
                sys.exit()
        return events

    def disable_display(self):
        self.display_enabled = False

    def render_stats(self, simulation):
        font_size = 15
        speed_ego_surface = generate_text_surface(
            f"Speed Ego: {simulation.ego.speed:.2f} m/s", COLORS["black"], font_size
        )
        reward_surface = generate_text_surface(
            f"Reward: {simulation.reward_cache:.2f}", COLORS["black"], font_size
        )

        throttle_surface = generate_text_surface(
            f"Throttle: {simulation.ego.inputAcceleration:.2f}",
            COLORS["black"],
            font_size,
        )

        steering_surface = generate_text_surface(
            f"Steering: {simulation.ego.inputSteering:.2f}", COLORS["black"], font_size
        )

        self.surf.blit(
            speed_ego_surface,
            (
                self.config.RENDER_WIDTH - speed_ego_surface.get_width(),
                30,
            ),
        )
        self.surf.blit(
            reward_surface,
            (
                self.config.RENDER_WIDTH - reward_surface.get_width(),
                speed_ego_surface.get_height() + 30,
            ),
        )
        self.surf.blit(
            throttle_surface,
            (
                self.config.RENDER_WIDTH - throttle_surface.get_width(),
                speed_ego_surface.get_height() + reward_surface.get_height() + 30,
            ),
        )

        self.surf.blit(
            steering_surface,
            (
                self.config.RENDER_WIDTH - steering_surface.get_width(),
                speed_ego_surface.get_height()
                + reward_surface.get_height()
                + throttle_surface.get_height()
                + 30,
            ),
        )

    def extract_stacked_rgb(self) -> np.array:
        obs = PIL.Image.fromarray(
            np.array(pygame.surfarray.pixels3d(self.surf))
            .astype(np.uint8)
            .transpose(1, 0, 2)
        )
        if self.config.CROP:
            obs = obs.crop((*self.config.CROP[0], *self.config.CROP[1]))
        obs = obs.resize(
            (self.config.OBSERVATION_SPACE_WIDTH, self.config.OBSERVATION_SPACE_HEIGHT)
        )
        if self.config.STACKED_BUFFER_SIZE > 1:
            obs = obs.convert("L")
            obs = np.expand_dims(np.array(obs), axis=0)
        else:
            obs = np.transpose(np.array(obs), (2, 0, 1))
        return obs

    def tick(self, simulation):
        if self.screen is None and self.display_enabled:
            pygame.init()
            self.screen = pygame.display.set_mode(
                (self.config.RENDER_WIDTH, self.config.RENDER_HEIGHT)
            )
            pygame.display.set_caption("Reinforcement Learning Environment")

        if self.display_enabled:
            self.event_cb()

        self.surf = pygame.Surface(
            (self.config.RENDER_WIDTH, self.config.RENDER_HEIGHT)
        )
        pygame.draw.rect(self.surf, (127, 127, 127), self.surf.get_rect())
        for entity in simulation.roads:
            entity.draw(self.surf)

        for entity in simulation.vehicles:
            entity.draw(self.surf)

        extracted = self.extract_stacked_rgb()
        simulation.obs = extracted

        if self.config.RECORDER_ENABLED:
            self.recorder.click(self.surf)

        if self.display_enabled:
            # draw vehicle speed limits in the top right corner
            self.render_stats(simulation)

            self.screen.fill((127, 127, 127))
            self.screen.blit(self.surf, (0, 0))
            pygame.display.flip()

        self.clock.tick(self.config.FPS)
        return extracted

    def reset(self):
        pass

    def close(self):
        if self.config.RECORDER_ENABLED:
            self.recorder.save()
        pygame.quit()
