import datetime
import uuid

import numpy as np

from src.utils import get_unique_filename, preload
from dataclasses import dataclass


class Config:
    def __init__(self, args):
        self.sim_params = preload(f"src/settings/{args.scene}/sim.yaml")
        self.RENDER_WIDTH = self.sim_params["RENDER_WIDTH"]
        self.RENDER_HEIGHT = self.sim_params["RENDER_HEIGHT"]
        self.OBSERVATION_SPACE_WIDTH = self.sim_params["OBSERVATION_SPACE_WIDTH"]
        self.OBSERVATION_SPACE_HEIGHT = self.sim_params["OBSERVATION_SPACE_HEIGHT"]
        self.STACKED_BUFFER_SIZE = args.stacked_buffer_size
        self.STEP_DURATION = self.sim_params["STEP_DURATION"]
        self.THROTTLE_MIN = self.sim_params["THROTTLE_MIN"]
        self.THROTTLE_MAX = self.sim_params["THROTTLE_MAX"]
        self.STEER_MIN = (
            eval(self.sim_params["STEER_MIN"])
            if isinstance(self.sim_params["STEER_MIN"], str)
            else self.sim_params["STEER_MIN"]
        )
        self.STEER_MAX = (
            eval(self.sim_params["STEER_MAX"])
            if isinstance(self.sim_params["STEER_MAX"], str)
            else self.sim_params["STEER_MAX"]
        )
        self.SPEED_LIMIT = self.sim_params["SPEED_LIMIT"]
        self.DIST_THRESHOLD = self.sim_params["DIST_THRESHOLD"]
        self.DT = self.sim_params["DT"]
        self.FPS = self.sim_params["FPS"]
        self.CROP = eval(self.sim_params["CROP"])

        self.SCENARIO_TYPE = args.scenario
        self.SCENE = args.scene
        self.SCENARIO_FILE = f"src/settings/{args.scene}/{args.scenario}.yaml"
        self.LOGGING_ENABLED = False
        self.MODE = args.mode
        self.DISPLAY_ENABLED = not args.offscreen
        self.RECORDER_ENABLED = args.record
        self.DEBUG = args.debug
        self.SIMPLE_RENDER = False
        self.RANDOMIZE_ADV_SPAWN = False
        self.hyperparameters = preload("src/settings/params.yaml")

        self.EXPERIMENT_DIR = f"data/{args.mode}_{args.scene}/exp_{args.scenario}_{args.output_prefix}-{get_unique_filename()}/"

    def dump(self):
        with open(f"{self.EXPERIMENT_DIR}/config.txt", "w") as f:
            for key, value in self.sim_params.items():
                print(f"{key}: {value}", file=f)

            for key, value in self.hyperparameters.items():
                print(f"{key}: {value}", file=f)


# def reload_config(config, args):
#     config.sim_params = preload(f"src/settings/{args.scene}/sim.yaml")
#     config.RENDER_WIDTH = config.sim_params["RENDER_WIDTH"]
#     config.RENDER_HEIGHT = config.sim_params["RENDER_HEIGHT"]
#     config.OBSERVATION_SPACE_WIDTH = config.sim_params["OBSERVATION_SPACE_WIDTH"]
#     config.OBSERVATION_SPACE_HEIGHT = config.sim_params["OBSERVATION_SPACE_HEIGHT"]
#     config.STACKED_BUFFER_SIZE = args.stacked_buffer_size
#     config.STEP_DURATION = config.sim_params["STEP_DURATION"]
#     config.THROTTLE_MIN = config.sim_params["THROTTLE_MIN"]
#     config.THROTTLE_MAX = config.sim_params["THROTTLE_MAX"]
#     config.STEER_MIN = (
#         eval(config.sim_params["STEER_MIN"])
#         if isinstance(config.sim_params["STEER_MIN"], str)
#         else config.sim_params["STEER_MIN"]
#     )
#     STEER_MAX = (
#         eval(config.sim_params["STEER_MAX"])
#         if isinstance(config.sim_params["STEER_MAX"], str)
#         else config.sim_params["STEER_MAX"]
#     )
#     config.SPEED_LIMIT = config.sim_params["SPEED_LIMIT"]
#     config.DIST_THRESHOLD = config.sim_params["DIST_THRESHOLD"]
#     config.DT = config.sim_params["DT"]
#     config.FPS = config.sim_params["FPS"]
#     config.CROP = eval(config.sim_params["CROP"])

#     config.SCENARIO_TYPE = args.scenario
#     config.SCENE = args.scene
#     config.SCENARIO_FILE = f"src/settings/{args.scene}/{args.scenario}.yaml"
#     config.LOGGING_ENABLED = False
#     config.MODE = args.mode
#     config.DISPLAY_ENABLED = not args.offscreen
#     config.RECORDER_ENABLED = args.record
#     config.DEBUG = args.debug
#     config.SIMPLE_RENDER = False
#     config.RANDOMIZE_ADV_SPAWN = False
#     config.hyperparameters = preload("src/settings/params.yaml")

#     config.EXPERIMENT_DIR = (
#         f"data/{args.mode}_{args.scene}/exp_{args.scenario}_{args.output_prefix}-{get_unique_filename()}/"
#     )
#     return config
