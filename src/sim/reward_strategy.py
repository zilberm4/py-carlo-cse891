from abc import ABC, abstractmethod

import numpy as np
from pygame.math import Vector2

from src.utils import map_to_range, preload, COLORS


def triangle_velocity_fitness(velocity, config):
    reward = 0
    if velocity > 0 and velocity <= config.SPEED_LIMIT:
        reward = (velocity) / (config.SPEED_LIMIT)
    elif velocity > config.SPEED_LIMIT and velocity < 80:
        reward = (80 - velocity) / 80
    return reward

def weave_velocity_fitness(velocity, config):
    reward = 0
    if velocity < 50:
        return -.1
    if velocity >= 50:
        return (velocity - 50)/50
    return reward

def average_roc_reward(vehicle, dt: float, config):
    denom = dt * len(vehicle.speed_history.cache)
    numer = vehicle.speed_history.cache[-1] - vehicle.speed_history.cache[0]
    avg_roc = numer / denom if denom != 0 else 0
    return abs(avg_roc)


def distance_driven_fitness(start, curr, end, config):
    if abs(end.y - curr.y) < 10:
        return 20
    total = np.linalg.norm(np.array(end) - np.array(start))
    curr_dist = np.linalg.norm(np.array(curr) - np.array(start))
    reward = curr_dist / total
    return reward

def weave_distance_driven_fitness(start, curr, end, config):
    dst = curr.distance_to(end)
    if dst < 20:
        return 20
   
    if (curr.y < 150 and curr.x > 515) or curr.y < -10:
        return -5

    if dst > 1000:
        dst = 1000
    
    dst = map_to_range(dst,0,1000,0,100)
    return 1/dst

def distance_between_vehicles_penalty(ego, adv, config):
    if ego is None or adv is None:
        return 0
    distance = np.linalg.norm(ego.center - adv.center)
    if distance > config.DIST_THRESHOLD:
        return 10
    return -1 * map_to_range(distance, 0, config.DIST_THRESHOLD, 100, 0)


def heading_offset_penalty(ego, config):
    target_heading = np.array([0, 1.5 * np.pi])
    target_heading = target_heading / np.linalg.norm(target_heading)
    if ego.speed < 1e-5:
        # if velocity is 0,0 avoid divide by zero
        return 0
    ego_heading = np.array([ego.velocity.x, ego.velocity.y])
    ego_heading = ego_heading / np.linalg.norm(ego_heading)
    angle = np.arccos(
        np.dot(ego_heading, target_heading)
        / (np.linalg.norm(ego_heading) * np.linalg.norm(target_heading))
    )
    # a = -map_to_range(angle, 0, np.pi, 0, 100)
    angle = np.rad2deg(angle) % 360  # convert to degrees
    angle /= 180  # normalize to 0-1
    return angle


def merge_road_reward(vehicle, config):
    lane_x = 425
    curr_x = vehicle.x
    dist = abs(lane_x - curr_x)
    dist = dist
    if dist > 25.0:
        return -0.01
    reward = 1 - dist / 25
    return reward


class RewardStrategy(ABC):
    @abstractmethod
    def compute_reward(self, config, simulation, vehicle, velocity, speed_penalty):
        pass


from abc import ABC, abstractmethod


class RewardStrategy(ABC):
    @abstractmethod
    def compute_reward(self, config, simulation, vehicle, velocity, speed_penalty):
        pass


class EgoRewardStrategy(RewardStrategy):
    def compute_reward(self, config, simulation, vehicle, velocity, speed_penalty):
        reward_tv = 0.2 * triangle_velocity_fitness(velocity, config)
        reward_dd = 1.2 * distance_driven_fitness(
            simulation.ego_start_pos, vehicle.center, simulation.target_end_pos, config
        )
        reward_dw = 0.5 * distance_between_vehicles_penalty(
            simulation.ego, simulation.adv, config
        )
        reward_ho = 0.5 * heading_offset_penalty(
            simulation.ego, config
        )  # TODO: set to 0 for curve
        return reward_tv + reward_dd + reward_ho + speed_penalty


class AdvRewardStrategy(RewardStrategy):
    def compute_reward(self, config, simulation, vehicle, velocity, speed_penalty):
        reward_tv = 0.2 * triangle_velocity_fitness(velocity, config)
        reward_dd = 1.2 * distance_driven_fitness(
            simulation.adv_start_pos, vehicle.center, simulation.target_end_pos, config
        )
        reward_dw = 0.1 * distance_between_vehicles_penalty(
            simulation.ego, simulation.adv, config
        )
        reward_ho = 0.15 * heading_offset_penalty(
            simulation.adv, config
        )  # TODO: set to 0 for curve
        reward = reward_tv + reward_dd + reward_ho + speed_penalty

        if not simulation.adv.crashed and simulation.ego.crashed:
            reward += 2

        reward -= (
            # TODO: Change relationship to not depend on EGO
            0.2
            * triangle_velocity_fitness(simulation.ego.speed, config)
        ) / 2  # slower ego, better adv
        return reward


class EgoRewardStrategyMerge(RewardStrategy):
    def compute_reward(self, config, simulation, vehicle, velocity, speed_penalty):
        reward_tv = 0.2 * triangle_velocity_fitness(velocity, config)
        reward_dd = 1.2 * distance_driven_fitness(
            simulation.ego_start_pos, vehicle.center, simulation.target_end_pos, config
        )
        reward_dw = 0.5 * distance_between_vehicles_penalty(
            simulation.ego, simulation.adv, config
        )
        reward_ho = 0.5 * heading_offset_penalty(
            simulation.ego, config
        )  # TODO: set to 0 for curve
        return reward_tv + reward_dd + reward_ho + speed_penalty


class AdvRewardStrategyMerge(RewardStrategy):
    def compute_reward(self, config, simulation, vehicle, velocity, speed_penalty):
        reward_tv = 0.2 * triangle_velocity_fitness(velocity, config)
        reward_dd = 1.2 * distance_driven_fitness(
            simulation.adv_start_pos, vehicle.center, simulation.target_end_pos, config
        )
        reward_dw = 0.1 * distance_between_vehicles_penalty(
            simulation.ego, simulation.adv, config
        )
        reward_ho = 0.15 * heading_offset_penalty(
            simulation.adv, config
        )  # TODO: set to 0 for curve
        reward = reward_tv + reward_dd + reward_ho + speed_penalty

        if not simulation.adv.crashed and simulation.ego.crashed:
            reward += 2

        reward -= (
            # TODO: Change relationship to not depend on EGO
            0.2
            * triangle_velocity_fitness(simulation.ego.speed, config)
        ) / 2  # slower ego, better adv
        return reward


class DistractedDriverStrategy(RewardStrategy):
    def compute_reward(self, config, simulation, vehicle, velocity, speed_penalty):
        reward_tv = 0.2 * triangle_velocity_fitness(velocity, config)
        reward_dd = 1.2 * distance_driven_fitness(
            simulation.adv_start_pos, vehicle.center, simulation.target_end_pos, config
        )
        reward_dw = 0  # 0.01 * distance_between_vehicles_penalty(simulation.ego, simulation.adv)
        reward_ho = -0.05 * heading_offset_penalty(
            simulation.adv, config
        )  # TODO: set to 0 for curve
        reward_avg_roc = average_roc_reward(vehicle, simulation.dt, config)
        reward = reward_tv + reward_dd + reward_ho + speed_penalty + reward_avg_roc

        if not simulation.adv.crashed and simulation.ego.crashed:
            reward += 2

        # reward -= (
        #     0.2 * triangle_velocity_fitness(simulation.ego.speed)
        # ) / 2  # slower ego, better adv
        return reward


class ACCAdvStrategy(RewardStrategy):
    def compute_reward(self, config, simulation, vehicle, velocity, speed_penalty):
        reward_tv = 0.2 * triangle_velocity_fitness(velocity, config)
        reward_dd = 1.2 * distance_driven_fitness(
            simulation.adv_start_pos, vehicle.center, simulation.target_end_pos, config
        )
        reward_dw = 0.1 * distance_between_vehicles_penalty(
            simulation.ego, simulation.adv, config
        )
        reward_ho = 0.01 * heading_offset_penalty(
            simulation.adv, config
        )  # TODO: set to 0 for curve
        reward_merge = 3.6 * merge_road_reward(vehicle, config)
        reward = reward_tv + reward_dd + reward_ho + speed_penalty + reward_merge

        if not simulation.adv.crashed and simulation.ego.crashed:
            reward += 2

        reward -= (
            # TODO: Change relationship to not depend on EGO
            0.2
            * triangle_velocity_fitness(simulation.ego.speed, config)
        ) / 2  # slower ego, better adv
        return reward

class AdvRewardStrategyWeave(RewardStrategy):
    def compute_reward(self, config, simulation, vehicle, velocity, speed_penalty):
        reward_tv = 2.0 * weave_velocity_fitness(velocity, config)
        reward_dd = 1.2 * weave_distance_driven_fitness(
            simulation.adv_start_pos, vehicle.center, simulation.target_end_pos, config
        )
        reward_dw = 0
        #0.01 * distance_between_vehicles_penalty(
        #     simulation.ego, simulation.adv, config
        # )
        reward_ho = 0
        reward = reward_tv + reward_dd + reward_ho + speed_penalty

        if not simulation.adv.crashed and simulation.ego.crashed:
            reward += .5

        reward -= (
            # TODO: Change relationship to not depend on EGO
            0.2
            * triangle_velocity_fitness(simulation.ego.speed, config)
        ) / 2  # slower ego, better adv
        return reward

REWARD_STRATEGIES = {
    "curve": {
        "ego": EgoRewardStrategy(),
        "adv": AdvRewardStrategy(),
        "ret": EgoRewardStrategy(),
    },
    "merge": {
        "ego": EgoRewardStrategyMerge(),
        "adv": AdvRewardStrategyMerge(),
        "ret": EgoRewardStrategyMerge(),
    },
    "weave": {
        "ego": EgoRewardStrategyMerge(),
        "adv": AdvRewardStrategyWeave(),
        "ret": EgoRewardStrategyMerge(),
    },
    "distracted": {
        "ego": EgoRewardStrategyMerge(),
        "adv": DistractedDriverStrategy(),
        "ret": EgoRewardStrategyMerge(),
    },
    "acc": {
        "ego": EgoRewardStrategyMerge(),
        "adv": ACCAdvStrategy(),
        "ret": EgoRewardStrategyMerge(),
    },
}
