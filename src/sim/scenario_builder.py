import json
import time

import numpy as np
from PIL import Image
from pygame.math import Vector2

from src.entities.road import Road
from src.entities.vehicle import (
    AutoCar,
    Car,
    PPOCar,
    WaypointCar,
    DistractedCar,
    AdaptiveCruiseCar,
    NPCCar,
)
from src.utils import COLORS, map_to_range, preload


class Scenario:
    def __init__(self, config):
        self.config = config
        self.scenario_data = preload(config.SCENARIO_FILE)
        self.train_mode = config.SCENARIO_TYPE

    def reload(self):
        self.scenario_data = preload(self.config.SCENARIO_FILE)
        self.train_mode = self.config.SCENARIO_TYPE

    def get_road_data(self):
        return self.scenario_data["road"]

    def get_agent_data(self):
        return self.scenario_data["vehicles"]


class AgentFactory:
    @staticmethod
    def create_agent(config, agent_data, **kwargs):
        should_spawn = agent_data.get("spawn", True)
        spawn_rate = agent_data.get("spawn_rate", 2)
        if not should_spawn or np.random.random() > spawn_rate:
            return None

        position = Vector2(*agent_data["position"][:])
        randomize_pos = agent_data.get("randomize", False)
        if randomize_pos:
            position[1] = position[1] + np.random.randint(-10, 150)

        if (
            agent_data.get("mode").lower() == "train"
            and config.MODE != "train"
        ):
            agent_data["mode"] = "ppo"

        angle = agent_data.get("heading", -np.pi / 2)
        velocity = agent_data["velocity"]
        controls = agent_data["controls"]
        mode = agent_data["mode"]
        role = agent_data["name"]
        color = "blue" if role == "ego" else "red"

        if mode == "ppo":
            model_path = agent_data["model"]
            v = PPOCar(
                position, angle, color=color, config=config, role=role, model=model_path
            )
        elif mode == "train":
            v = Car(position, angle, color=color, config=config, role=role)
        elif mode == "auto":
            v = AutoCar(position, angle, color=color, config=config, role=role)
        elif mode == "waypoint":
            road = kwargs.get("road")
            if not road:
                raise ValueError("Waypoint car needs a road")
            waypoints = road.waypoints
            v = WaypointCar(
                position, angle, waypoints, color=color, config=config, role=role
            )
        elif mode == "distracted":
            model_path = agent_data["model"]
            v = DistractedCar(
                position, angle, color=color, config=config, role=role, model=model_path
            )
        elif mode == "acc":
            acc_type = agent_data.get("acc_type", "old")
            v = AdaptiveCruiseCar(
                position,
                angle,
                color=color,
                config=config,
                role=role,
                acc_type=acc_type,
            )
        elif mode == "npc":
            angle = -np.pi / 2
            if (
                config.SCENE == "weave"
            ):  # TODO: again, this is bad, should be in sim file
                velocity += np.random.randint(-5, 10)
            v = NPCCar(
                position,
                angle,
                color=color,
                config=config,
                role=role,
                init_speed=Vector2(velocity, 0),
            )

        v.velocity = Vector2(velocity, 0)
        v.set_control(*controls)
        return v
