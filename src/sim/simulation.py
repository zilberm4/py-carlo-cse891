import json
import time

import numpy as np
from PIL import Image
from pygame.math import Vector2
from src.entities.road import Road, RoadAsImage
from src.entities.vehicle import Car, PPOCar, AutoCar
from src.utils import map_to_range, preload, COLORS
from src.sim.scenario_builder import Scenario, AgentFactory
from src.sim.reward_strategy import (
    EgoRewardStrategy,
    AdvRewardStrategy,
    EgoRewardStrategyMerge,
    AdvRewardStrategyMerge,
    DistractedDriverStrategy,
    REWARD_STRATEGIES,
)


class Simulation:
    @property
    def vehicles(self):
        return self.actor_list["vehicles"]

    @property
    def npcs(self):
        return list(filter(lambda x: x.role == "npc", self.actor_list["vehicles"]))

    @property
    def trainable_vehicle(self):
        return self.ego if self.train_mode != "adv" else self.adv

    @property
    def roads(self):
        return self.actor_list["roads"]

    def __init__(self, config):
        self.config = config
        self.actor_list = {"vehicles": [], "pedestrians": [], "roads": []}
        self.width, self.height = self.config.RENDER_WIDTH, self.config.RENDER_HEIGHT
        self.dt = self.config.DT
        self.xoff = 50
        self.obs = np.zeros(
            (
                3,
                self.config.OBSERVATION_SPACE_WIDTH,
                self.config.OBSERVATION_SPACE_HEIGHT,
            ),
            dtype=np.uint8,
        )
        self.scenario = None
        self.train_mode = self.config.SCENARIO_TYPE
        self.reward_strategies = REWARD_STRATEGIES[self.config.SCENE]
        self.ego = None
        self.adv = None
        self.reward_cache = -99999
        self.setup()

    def setup(self):
        self.scenario = Scenario(self.config)
        self.create_roads(self.scenario.get_road_data())
        self.spawn_agents()
        self.start_time = time.time()

    def create_roads(self, road_data):
        if "segments" in road_data:
            road_points = road_data["segments"]
            road_points = list(map(lambda x: Vector2(x[0], x[1]), road_points))
            rd = Road(self.config, road_points)
            lane_marking_points = road_data["markings"]
            lane_marking_points = list(
                map(lambda x: Vector2(x[0], x[1]), lane_marking_points)
            )
            rd.add_lane_markings(
                lane_marking_points,
            )
            self.roads.append(rd)
        else:
            rd = RoadAsImage(self.config, road_data)
            self.roads.append(rd)

    def get_non_acc_vehicles(self):
        # LIMITS TO 1 ACC in simulation
        return list(filter(lambda x: x.mode != "acc", self.actor_list["vehicles"]))

    def spawn_npcs(self, agent_data):
        kwargs = {"road": self.roads[0]}
        for agent in agent_data:
            if agent["name"] == "ego" or agent["name"] == "adv":
                continue
            npc = AgentFactory.create_agent(self.config, agent, **kwargs)
            self.actor_list["vehicles"].append(npc)

    def spawn_adv(self, agent_data):
        kwargs = {"road": self.roads[0]}
        adv_raw = list(filter(lambda x: x["name"] == "adv", agent_data))
        if adv_raw:
            adv = AgentFactory.create_agent(self.config, adv_raw[0], **kwargs)
            if adv:
                self.adv = adv
                self.actor_list["vehicles"].append(self.adv)
                self.adv_start_pos = self.adv.center if self.adv else None

    def spawn_ego(self, agent_data):
        kwargs = {"road": self.roads[0]}
        ego_raw = list(filter(lambda x: x["name"] == "ego", agent_data))
        if ego_raw:
            ego = AgentFactory.create_agent(self.config, ego_raw[0], **kwargs)
            if ego:
                self.ego = ego
                self.actor_list["vehicles"].append(self.ego)
                self.ego_start_pos = self.ego.center
                if ego_raw[0]["mode"] == "acc":
                    self.ego.other_vehicles = self.get_non_acc_vehicles()

    def spawn_agents(self):
        agent_data = self.scenario.get_agent_data()
        self.spawn_npcs(agent_data)
        self.spawn_adv(agent_data)
        self.spawn_ego(agent_data)

        # TODO: make this more modular
        if self.config.SCENE == "curve":
            self.target_end_pos = Vector2(
                self.config.RENDER_WIDTH, self.roads[0].waypoints[-1].y
            )
        elif self.config.SCENE == "weave":
            self.target_end_pos = Vector2(375, 0)
        else:
            self.target_end_pos = Vector2(self.ego_start_pos.x, 0)

    def check_vehicle_road_collisions(self):
        for road in self.roads:
            for vehicle in self.vehicles:
                if len(vehicle.collision_history) == 0:
                    collides = road.checkCollisions(vehicle)
                    if collides:
                        vehicle.collision_history.append(1)

    def check_vehicle_vehicle_collisions(self):
        if self.ego and self.adv:
            if self.ego.collides(self.adv):
                self.ego.collision_history.append(1)
                self.adv.collision_history.append(1)
                return True

        npcs = self.npcs
        for npc in npcs:
            if self.ego and self.ego.collides(npc):
                self.ego.collision_history.append(1)
                npc.collision_history.append(1)
            if self.adv and self.adv.collides(npc):
                self.adv.collision_history.append(1)
                npc.collision_history.append(1)

    def process_vehicles(self):
        for vehicle in self.vehicles:
            vehicle.tick(self.dt, self.obs)

    def post_process_vehicles(self):
        for i, vehicle in enumerate(self.vehicles):
            if len(vehicle.collision_history) > 0:
                if type(vehicle) != AutoCar:
                    # set collided vehicle to crashed state
                    vehicle.color = COLORS["red"]
                    vehicle.velocity = Vector2(0, 0)
                    vehicle.crashed = True

            # set finished state if vehicle has crossed finish line
            if vehicle.center.distance_to(self.target_end_pos) < vehicle.height:
                vehicle.velocity = Vector2(0, 0)
                vehicle.color = COLORS["green"]
                vehicle.finished = True

    def tick(self):
        self.check_vehicle_road_collisions()
        self.check_vehicle_vehicle_collisions()
        self.process_vehicles()
        self.post_process_vehicles()

    def apply_action(self, vehicle, action):
        throttle, steer = action[0], action[1]
        vehicle.set_control(throttle, steer)
        self.tick()
        return vehicle.speed

    def step(self, action):
        vehicle = self.trainable_vehicle
        velocity = self.apply_action(vehicle, action)

        done = False
        speed_penalty = 0
        if velocity < 1e-2:
            done = True
            speed_penalty = -5

        info = {
            "ego_speed": self.ego.speed,
            "adv_speed": self.adv.speed if self.adv else 0,
            "ego_state": self.ego.state,
            "adv_state": self.adv.state if self.adv else 0,
        }
        reward = 0
        if len(vehicle.collision_history) > 0:
            done = True
            reward = -50
            return None, reward, done, info

        if vehicle.finished:
            done = True
            reward = 100
            return None, reward, done, info

        reward_strategy = self.reward_strategies[self.train_mode]
        reward = reward_strategy.compute_reward(
            self.config, self, vehicle, velocity, speed_penalty
        )

        self.reward_cache = reward
        return None, reward, done, info

    def reset(self):
        del self.ego
        del self.adv
        self.actor_list = {"vehicles": [], "pedestrians": [], "roads": []}
        self.ego = None
        self.adv = None
        self.reward_cache = -99999
        self.setup()
