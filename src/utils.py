import datetime
import json
import os
import uuid
import yaml
import shutil

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pygame

import argparse

# fmt: off
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--mode","-m",type=str,default="train",dest="mode",choices=['train','test','eval', 'manual', 'plot'], help="train, test, eval, manual, plot",)
    parser.add_argument("--scene",type=str,default="",dest="scene",choices=['merge','curve','acc','weave', 'distracted'], help="Specifies which traffic scene to load. Example: --scene curve",)
    parser.add_argument("--data","-d",type=str,default="data/",dest="data_path",help="Path to .csv log file for plotting.",)
    parser.add_argument("--checkpoint","--model","-c","--ckpt",type=str,default=None,dest="checkpoint",help="Enter path of model .pth file",)
    parser.add_argument("-r","--resume",action="store_true",dest="resume",help="Resume training from checkpoint",)
    parser.add_argument("--offscreen",action="store_true",  dest="offscreen",help="Pass this flag to render offscreen",)# false if argument is not passed
    parser.add_argument("--record",action="store_true",dest="record",help="Record video of gameplay",)
    parser.add_argument("--output-prefix",type=str,default="",dest="output_prefix",help="Add prefix to output dir.",)
    parser.add_argument("--save-freq", type=int, default=int(1e5), help="Save model frequency")
    parser.add_argument("--total-timesteps", type=int, default=int(3e6), help="Total timesteps")
    parser.add_argument("--tensorboard",action="store_true",dest="tensorboard",help="Enable tensorboard",)
    parser.add_argument("--debug",action="store_true",dest="debug",help="Enable debug printing",)
    parser.add_argument("--scenario",dest="scenario",default="ego", choices=['ego', 'adv', 'ret'], help="Specificy which agent to train. Options are 'ego' or 'adv' or 'ret",)
    parser.add_argument("--n-evals",dest="n_evals",type=int, default=25, help="How many episodes to evaluate.")
    
    # hyperparameters
    parser.add_argument("--frame-stack",type=int,default=1,dest="stacked_buffer_size",help="Number of frames to stack per observation. Default is 1.",)
    parser.add_argument("--learning_rate", type=float, default=0.0003, help="Learning rate")
    parser.add_argument("--n_steps", type=int, default=2048, help="Number of steps per rollout")
    parser.add_argument("--batch_size", type=int, default=128, help="Batch size")
    parser.add_argument("--n_epochs", type=int, default=10, help="Number of epochs")
    parser.add_argument("--gamma", type=float, default=0.99, help="Discount factor")
    parser.add_argument("--gae_lambda", type=float, default=0.95, help="GAE lambda")
    parser.add_argument("--clip_range", type=float, default=0.1, help="Clipping range")
    parser.add_argument("--ent_coef", type=float, default=0.0, help="Entropy coefficient")
    parser.add_argument("--vf_coef", type=float, default=0.5, help="Value function coefficient")
    return parser.parse_args()
# fmt: on


def get_unique_filename():
    return (
        f"{datetime.datetime.now().strftime('%m-%d-%H-%M-%S')}-{str(uuid.uuid4())[:3]}"
    )


def ensure_dir_structure(dirname: str):
    if not os.path.exists(dirname):
        os.makedirs(dirname)


def preload(filename):
    try:
        with open(filename, "r") as f:
            yaml_data = yaml.safe_load(f)
            return yaml_data
    except FileNotFoundError:
        return None


def rename_to_mode(path: str, mode: str):
    if not os.path.exists(path):
        return

    path_parts = path.split("/")
    path_parts[-1] = f"{mode}_{path_parts[-1]}"
    new_path = "/".join(path_parts)
    os.rename(path, new_path)
    return new_path


####################### simulation helpers ##########################################################
def ccw(A, B, C):
    # https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect
    return (C.y - A.y) * (B.x - A.x) > (B.y - A.y) * (C.x - A.x)


def intersect(A, B, C, D):
    # https://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect
    return ccw(A, C, D) != ccw(B, C, D) and ccw(A, B, C) != ccw(A, B, D)


def map_to_range(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


#####################################################################################################


####################### render helpers ##############################################################
def draw_line_dashed(
    surface, color, start_pos, end_pos, width=1, dash_length=10, exclude_corners=True
):
    start_pos = np.array(start_pos)
    end_pos = np.array(end_pos)
    length = np.linalg.norm(end_pos - start_pos)
    dash_amount = int(length / dash_length)
    dash_knots = np.array(
        [np.linspace(start_pos[i], end_pos[i], dash_amount) for i in range(2)]
    ).transpose()
    return [
        pygame.draw.line(
            surface, color, tuple(dash_knots[n]), tuple(dash_knots[n + 1]), width
        )
        for n in range(int(exclude_corners), dash_amount - int(exclude_corners), 2)
    ]


COLORS = {
    "orange": (255, 165, 0),
    "red": (255, 0, 0),
    "green": (0, 255, 0),
    "blue": (0, 0, 255),
    "black": (0, 0, 0),
    "white": (255, 255, 255),
    "yellow": (255, 255, 0),
    "purple": (128, 0, 128),
}
#####################################################################################################
